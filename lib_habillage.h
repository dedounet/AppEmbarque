#ifndef _LIB_HABILLAGE_H_
#define _LIB_HABILLAGE_H_
/***************************************************************************
* Nom du prg 	:   librairie g�n�rale de gestion du panneau
* Description	:   
*
* Auteur        :   FT
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:
***************************************************************************/
#include <xc.h>
#include "lib_PCA9685.h"
#include <string.h>
#include "lib_24FC1025.h"
#include "eusart1.h"
#include <stdlib.h>


//#define     OSCILLATEUR_INTERNE

#define TRIS_RESET_XBEE     TRISBbits.TRISB4
#define RESET_XBEE          LATBbits.LATB4

#define TRIS_CMD_CAPT_LUM   TRISBbits.TRISB3
#define CMD_CAPT_LUM        LATBbits.LATB3
    

//--------------------------------------------
#define TIC_10MS        63036       // 10ms
#define TIC_20MS        60536       // 20ms
#define TIC_25MS        59286       // 25ms
#define TIC_50MS        53036       // 50ms
#define TIC_100MS       40536       // 100 ms
#define TIC_200MS       15536       // 200 ms
#define TIC_250MS        3036       // 250 ms

#define TIC_BANDEAUX    TIC_100MS
//--------------------------------------------
#define TIC_5SEC        26474       // 5 secondes

//#define NB_FOIS_5SEC    720     // 1h = 720 fois 5s
#define NB_FOIS_5SEC    1     
//--------------------------------------------
#define NB_MOTIF_MAX    204      // 204 pas max par prog lumineux
//--------------------------------------------
// Codes d'erreur sp�cifiques

#define PROG_OK                  0
#define NB_PROG_OUT_OF_RANGE    -1
#define EMPTY_PROG              -2
#define PROG_SIZE_OUT_OF_RANGE  -3

//--------------------------------------------
//UART


//----------------------------------------------
//Types

/*-- Enum de la machine d'�tat */
typedef enum {
    BASE_AFF=0, AFF_MOTIF=1,EXEC_PROG= 2,STOP_PROG= 3,SAV_PROG=4, CURRENT_PROG=6, LUM=8, TEST_COM= 10,DEBUG=11
}tEtat;

//----------------------------------------------

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    This macro will enable global interrupts.
 * @Example
    INTERRUPT_GlobalInterruptEnable();
 */
#define INTERRUPT_GlobalInterruptEnable() (INTCONbits.GIE = 1)

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    This macro will disable global interrupts.
 * @Example
    INTERRUPT_GlobalInterruptDisable();
 */
#define INTERRUPT_GlobalInterruptDisable() (INTCONbits.GIE = 0)
/**
 * @Param
    none
 * @Returns
    none
 * @Description
    This macro will enable peripheral interrupts.
 * @Example
    INTERRUPT_PeripheralInterruptEnable();
 */
#define INTERRUPT_PeripheralInterruptEnable() (INTCONbits.PEIE = 1)

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    This macro will disable peripheral interrupts.
 * @Example
    INTERRUPT_PeripheralInterruptDisable();
 */
#define INTERRUPT_PeripheralInterruptDisable() (INTCONbits.PEIE = 0)




//--------------------------------------------
typedef struct
{
    unsigned int Duree;
    unsigned char   Intensites[15];
} tMotif;
/*--------------------------------------------------------------------------*/
/* Fonction  : void Initialiser(void)    									*/
/* Description : Configuration ressources                                   */
/* PE : Aucune                                                              */
/* ps : Aucune                                                              */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
void Initialiser(void);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE : N� du programme en EEPROM, entre 1 et 64                            */
/*      Pointeur sur l'adresse de la variable recueillant l'info            */
/* ps : -1 : erreur                                                         */
/*      0 : OK, l'adresse EEPROM est en bank 0                              */
/*      1 : OK, l'adresse EEPROM est en bank 1                              */
/* Mode d'emploi :  L'adresse est donn�e sur 16 bits, le 17eme est la valeur*/
/*                  du param�tre de sortie (0 ou 1)                         */
/*--------------------------------------------------------------------------*/
signed char GetAdrProgLumEEPROM(unsigned char NbProg, unsigned int* pAdr);


/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :    N� du programme en EEPROM, entre 1 et 64                         */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char     LoadProgLumFromEEPROM(unsigned char NbProg);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char     SaveProgActiveToEEPROM(unsigned char NbProg);


/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char     LoadMotifEEPROM(unsigned int AdrMotif,tBank Bank,unsigned char* pData);


/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :     IdMotif entre 0 et 203                                          */
/*          Adresse de la structure tMotif Cible                            */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char     ConvertirRawToMotif(unsigned char IdMotif,tMotif* pMotif);


/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char     GenererMotif(tMotif* pMotif);



/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description : "Efface" un programme lumineux.                            */
/*              En r�alit�, place la valeur 0xFF dans l'octet "size"        */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char LibererProgrammeEEPROM(unsigned char NbProg);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char TesterPresenceProgrammeEEPROM(unsigned char NbProg);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
char TestXbee(void);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
int TraitementUart(DataUart* pDataUart, tEtat* mode);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
void videBuffer(DataUart *pBuff);

#endif