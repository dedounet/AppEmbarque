/***************************************************************************
* Nom du prg 	:  lib_i2c_V2.c
* Description	: 
*
* Auteur	: 		Alexis ROLLAND
* Cr�� le	: 		16/11/2007
* Compilateur	: XC8
***************************************************************************
* Modifi� le  	: 09/2015 - Portage sur PIC18F26K22
***************************************************************************/

/* inclusion des fichiers ent�te .h 	*/
#include "lib_i2c_18F26K22.h"

/* D�clarations des variables globales 	*/


/**************************************************************************/
void I2C_Init(void)
{

// Config lifgned I2C en entr�es # (cf doc)
TRISCbits.TRISC3 = 1;
TRISCbits.TRISC4 = 1;
ANSELCbits.ANSC3 = 0;
ANSELCbits.ANSC4 = 0;


PIE1bits.SSP1IE = 0;
SSP1ADD = 24;		// Fixe la vitesse du bus � 100kHz pour Q = 8MHz
					// Pour une autre fr�quence ou un autre Quartz
					// SSPADD = (Fosc / 4.Clk) - 1

SSP1STAT = 0x80;		// Devient 0x00 pour Clk = 400 kHz (0x80 : 100kHz)
SSP1CON1 = 0x28;
SSP1CON2 = 0x00;


return;
}
/**************************************************************************/
signed char I2C_Write_Byte(unsigned char Adr,unsigned char Byte)
{
SSP1CON2bits.SEN = 1;				// Condition de start
while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
PIR1bits.SSP1IF = 0;

// Adresse
SSP1BUF = (Adr & 0xFE);			// M�morisation Adresse
while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF

if (SSP1CON2bits.ACKSTAT != 0)
	{
	SSP1CON2bits.PEN = 1;				// Condition de STOP
	while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
	PIR1bits.SSP1IF = 0;
	return I2C_NO_ACK;
	}
PIR1bits.SSP1IF = 0;

// Octet
SSP1BUF = Byte;
while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
if (SSP1CON2bits.ACKSTAT != 0)
	{
	SSP1CON2bits.PEN = 1;				// Condition de STOP
	while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
	PIR1bits.SSP1IF = 0;
	return I2C_NO_ACK;
	}

PIR1bits.SSP1IF = 0;

SSP1CON2bits.PEN = 1;				// Condition de STOP
while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
PIR1bits.SSP1IF = 0;
return I2C_OK;
}


signed char	I2C_Read_Byte(unsigned char Adr,unsigned char *pByte)
{
unsigned char Octet;

SSP1CON2bits.SEN = 1;           // Condition de start
while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
PIR1bits.SSP1IF = 0;

// Adresse
SSP1BUF = (Adr | 0x01);

while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
if (SSP1CON2bits.ACKSTAT !=0)
        {
	SSP1CON2bits.PEN = 1;				// Condition de STOP
	while (!PIR1bits.SSP1IF);			// Attente drapeau SSPIF
	PIR1bits.SSP1IF = 0;
	return I2C_NO_ACK;
	}
PIR1bits.SSP1IF = 0;

SSP1CON2bits.RCEN = 1;				// Mode Rx

while (!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;

Octet = SSP1BUF;

SSP1CON2bits.ACKEN = 1;
SSP1CON2bits.ACKDT = 0;

while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;

SSP1CON2bits.PEN = 1;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;

*pByte = Octet;
return I2C_OK;
}


signed char	I2C_PutByte(unsigned char Byte)
{
SSP1BUF = Byte;
while(!PIR1bits.SSP1IF);

if (SSP1CON2bits.ACKSTAT != 0)
	{
	
	SSP1CON2bits.PEN = 1;
	while(!PIR1bits.SSP1IF);
	PIR1bits.SSP1IF = 0;
	return I2C_NO_ACK;
	}
PIR1bits.SSP1IF = 0;
return I2C_OK;
}

signed char	I2C_GetByte(unsigned char *pByte,unsigned char EtatACK)
{
SSP1CON2bits.ACKDT = EtatACK;

SSP1CON2bits.RCEN = 1;
while(!PIR1bits.SSP1IF);

*pByte = SSP1BUF;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;

SSP1CON2bits.ACKEN = 1;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;

return I2C_OK;
}

void	I2C_Start(void)
{
SSP1CON2bits.SEN = 1;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;
}

void	I2C_Stop(void)
{
SSP1CON2bits.PEN = 1;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;
}

void	I2C_Restart(void)
{
SSP1CON2bits.RSEN = 1;
while(!PIR1bits.SSP1IF);
PIR1bits.SSP1IF = 0;
}


