/***************************************************************************
* Nom du prg    :   Gestion du panneau lumineux Polytech
* Description   :   Programme de gestion du panneau d'affichage lumineux, il
*                   permet de communiquer avec l'application de contr�le d�port�.  
* Auteur        :   Franck Teyssier
* Cr�� le       :   
* Compilateur	:   XC8
***************************************************************************
* Modifi� le  	:   23/02/2018
***************************************************************************/

#include "lib_habillage.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>


/*-------D�finitions-----------*/


/*----------------------------------------------------------------------------*/
/*Ins�rer Ici les bits de configuration pour le �C 							  */				  
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
//  18F26K22
/*----------------------------------------------------------------------------*/

// CONFIG1H
#pragma config FOSC = HSMP    // Oscillator Selection bits->HS oscillator (medium power 4-16 MHz)
#pragma config PLLCFG = OFF    // 4X PLL Enable->Oscillator used directly
#pragma config PRICLKEN = ON    // Primary clock enable bit->Primary clock enabled
#pragma config FCMEN = OFF    // Fail-Safe Clock Monitor Enable bit->Fail-Safe Clock Monitor disabled
#pragma config IESO = OFF    // Internal/External Oscillator Switchover bit->Oscillator Switchover mode disabled

// CONFIG2L
#pragma config PWRTEN = OFF    // Power-up Timer Enable bit->Power up timer disabled
#pragma config BOREN = SBORDIS    // Brown-out Reset Enable bits->Brown-out Reset enabled in hardware only (SBOREN is disabled)
#pragma config BORV = 190    // Brown Out Reset Voltage bits->VBOR set to 1.90 V nominal

// CONFIG2H
#pragma config WDTEN = OFF    // Watchdog Timer Enable bits->Watch dog timer is always disabled. SWDTEN has no effect.
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits->1:32768

// CONFIG3H
#pragma config CCP2MX = PORTC1    // CCP2 MUX bit->CCP2 input/output is multiplexed with RC1
#pragma config PBADEN = ON    // PORTB A/D Enable bit->PORTB<5:0> pins are configured as analog input channels on Reset
#pragma config CCP3MX = PORTB5    // P3A/CCP3 Mux bit->P3A/CCP3 input/output is multiplexed with RB5
#pragma config HFOFST = ON    // HFINTOSC Fast Start-up->HFINTOSC output and ready status are not delayed by the oscillator stable status
#pragma config T3CMX = PORTC0    // Timer3 Clock input mux bit->T3CKI is on RC0
#pragma config P2BMX = PORTB5    // ECCP2 B output mux bit->P2B is on RB5
#pragma config MCLRE = EXTMCLR    // MCLR Pin Enable bit->MCLR pin enabled, RE3 input pin disabled

// CONFIG4L
#pragma config STVREN = ON    // Stack Full/Underflow Reset Enable bit->Stack full/underflow will cause Reset
#pragma config LVP = ON    // Single-Supply ICSP Enable bit->Single-Supply ICSP enabled if MCLRE is also 1
#pragma config XINST = OFF    // Extended Instruction Set Enable bit->Instruction set extension and Indexed Addressing mode disabled (Legacy mode)
#pragma config DEBUG = OFF    // Background Debug->Disabled

// CONFIG5L
#pragma config CP0 = OFF    // Code Protection Block 0->Block 0 (000800-003FFFh) not code-protected
#pragma config CP1 = OFF    // Code Protection Block 1->Block 1 (004000-007FFFh) not code-protected
#pragma config CP2 = OFF    // Code Protection Block 2->Block 2 (008000-00BFFFh) not code-protected
#pragma config CP3 = OFF    // Code Protection Block 3->Block 3 (00C000-00FFFFh) not code-protected

// CONFIG5H
#pragma config CPB = OFF    // Boot Block Code Protection bit->Boot block (000000-0007FFh) not code-protected
#pragma config CPD = OFF    // Data EEPROM Code Protection bit->Data EEPROM not code-protected

// CONFIG6L
#pragma config WRT0 = OFF    // Write Protection Block 0->Block 0 (000800-003FFFh) not write-protected
#pragma config WRT1 = OFF    // Write Protection Block 1->Block 1 (004000-007FFFh) not write-protected
#pragma config WRT2 = OFF    // Write Protection Block 2->Block 2 (008000-00BFFFh) not write-protected
#pragma config WRT3 = OFF    // Write Protection Block 3->Block 3 (00C000-00FFFFh) not write-protected

// CONFIG6H
#pragma config WRTC = OFF    // Configuration Register Write Protection bit->Configuration registers (300000-3000FFh) not write-protected
#pragma config WRTB = OFF    // Boot Block Write Protection bit->Boot Block (000000-0007FFh) not write-protected
#pragma config WRTD = OFF    // Data EEPROM Write Protection bit->Data EEPROM not write-protected

// CONFIG7L
#pragma config EBTR0 = OFF    // Table Read Protection Block 0->Block 0 (000800-003FFFh) not protected from table reads executed in other blocks
#pragma config EBTR1 = OFF    // Table Read Protection Block 1->Block 1 (004000-007FFFh) not protected from table reads executed in other blocks
#pragma config EBTR2 = OFF    // Table Read Protection Block 2->Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks
#pragma config EBTR3 = OFF    // Table Read Protection Block 3->Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks

// CONFIG7H
#pragma config EBTRB = OFF    // Boot Block Table Read Protection bit->Boot Block (000000-0007FFh) not protected from table reads executed in other blocks


/*----------------------------------------------------------------------------*/
/* D�clarations des variables globales 	*/

uint16_t Nb5Sec = 0, NbTic100=0, VitClignot=10, toggleClignot=0;
char    AcqLum = 0, Tic100ms = 0, flag_receive=0;
persistent char freqPCA=60 ;
tEtat   mode =0;    //var machine etat

uint16_t tab_Toff [16]= {1000,1000,1000,1000,   //tableau des luminosit� (Toff))
                            1000,1000,1000,1000,
                            1000,1000,1000,1000,
                            1000,1000,1000,1000};

//Donn�es de message UART
DataUart DataIn = {{0},0};
DataUart DataOut= {{0},0};


/* Directives de compilation		*/

/* Interruptions haute priorit�		*/
static void interrupt ItHigh(void)
{
     if(INTCONbits.PEIE == 1)
    {
        if (PIR1bits.TMR1IF && PIE1bits.TMR1IE) // IT timer 100ms
        {
            WRITETIMER1(TIC_BANDEAUX);
            NbTic100++;
            Tic100ms = 1;
            PIR1bits.TMR1IF = 0;
            if ((NbTic100==VitClignot) && toggleClignot){
                NbTic100 =0;
                PCA9685_OutputToggle();
            }
                
        }
        
        
        if(PIE1bits.RC1IE == 1 && PIR1bits.RC1IF == 1)
        {
            EUSART1_Receive_ISR();
            flag_receive=1;
        } 
        
        else if(PIE1bits.TX1IE == 1 && PIR1bits.TX1IF == 1)
        {
            EUSART1_Transmit_ISR();
        } 
        
     }
       
}

/* Interruptions Basse priorit�	*/
static void interrupt low_priority ItLow(void)
{
    if ((INTCONbits.TMR0IF)&&(INTCONbits.TMR0IE))
    {
       WRITETIMER0(TIC_5SEC);
       
       if (Nb5Sec < (NB_FOIS_5SEC-1))
       {
           Nb5Sec++;
       }
       else
       {
          /* CMD_CAPT_LUM  = 1;
           ADCON0bits.GODONE = 1;   // D�clenchement CAN*/
           
           Nb5Sec = 0;
           
       }
       INTCONbits.TMR0IF = 0;
    }
    
    if (PIR1bits.ADIF && PIE1bits.ADIE)
    {
        
        AcqLum = 1;
        PIR1bits.ADIF = 0;
    }
    
}

/* Programme Principal			*/
void main(void)
{
    char Luminosite, pass=1;
    uint16_t i =0 ,Toff_debug = 0, varDebug=0;
    uint8_t message[]= {"Hello, panneau high tech par Franck Teyssier!\n"};
 
    
    Initialiser();
    
    INTERRUPT_PeripheralInterruptEnable();
    INTERRUPT_GlobalInterruptEnable();
    
    while (!Tic100ms); // attente init Xbee apr�s reset
    Tic100ms=0;
    
    /*---- Tests des fonctions -----*/
    //--------------UART------------//
    for (i=0; message[i]!= '\0' ;i++)
    EUSART1_Write(message[i]);
    
    //--------------Leds flash--------//
    /*PCA9685_LedAll(0,1000); 
    i=0;
    while (!Tic100ms && i<=10){}
    PCA9685_LedAll(0,0);*/
    
#define TEST    //active les tests de validation
/*------Le mode Test (debug) fonctionne , mais pas  impl�ment� correctement dans le normal.
 *
 * */
    
while(1)
    {
    
#ifdef TEST
        
    /*---Validation UART---*/
    if (flag_receive){ 
        EUSART1_stringRead(&DataIn);
        //EUSART1_stringWrite(&DataIn.data);    //test reception/envoi de caract�re
        TraitementUart(&DataIn, &mode);
        pass=0;
        videBuffer(&DataIn.data);
        flag_receive=0;
    }
    
     /*---Validation I2C---*/
    /*---Validation machine d'etat---*/
    switch (mode) {
        case 0 :
            if(!pass){
            for (i=0 ; i<= 15; i++)
                PCA9685_SetLedPattern(i,0, tab_Toff[i]);
            pass=1;
            }
                     
            break;
        case 1: 
            for (i=0 ; i<= 15; i++)
                tab_Toff[i] += 100;
            mode =0 ;
            break;
        case 2:
            for (i=0 ; i<= 15; i++)
                tab_Toff[i] -= 100;
            mode =0 ;
            break;
        case 3:
            Toff_debug = EUSART1_toIntRead(&DataIn);
            for (i=0 ; i<= 15; i++)
                tab_Toff[i]= Toff_debug;
            pass=0;            
            mode =0 ;
            break;
        case 4: 
            freqPCA += 5;
            PCA9685_SetPrescaler(freqPCA);
            mode =0 ;
            break;
        case 5: 
            freqPCA -= 5;
            PCA9685_SetPrescaler(freqPCA);
            mode =0 ;
            break;
        case 6:
            for(i=0; i<16; i++)
                EUSART1_Write( tab_Toff[i] );
            EUSART1_Write(CHAR_ENDL);
            mode =0 ;
            break;
        case 7 : 
            freqPCA= EUSART1_toIntRead(&DataIn);
            PCA9685_SetPrescaler( freqPCA);
            pass= 0;
            mode= 0;      
            break;
        case TEST_COM:
            EUSART1_stringWrite("ok");
            mode =0 ;
        break;
        
        default:  
            EUSART1_stringWrite("error");
            for (i=0 ; i<= 15; i++)
                tab_Toff[i] =1000;
            mode =0 ;
        break;
    }
#else  
    
    /*----Traitement des donn�es entrantes en uart*/
    if(flag_receive){
        EUSART1_stringRead(&DataIn);
        TraitementUart(&DataIn, &mode);
        videBuffer(&DataIn.data);
        pass=0;
        flag_receive=0;
    }
    
    /*----traitement des donn�es re�ues par une machine d'�tat---*/
     switch (mode) {
        case BASE_AFF :
            if(!pass){
            for (i=0 ; i<= 15; i++)
                PCA9685_SetLedPattern(i,0, tab_Toff[i]);
            pass=1;
            }
            break;
        case AFF_MOTIF: 
            mode= BASE_AFF;
            
            break;
        case EXEC_PROG:
            mode= BASE_AFF;
            break;
        case STOP_PROG:
            mode= BASE_AFF;
            break;
        case SAV_PROG:
             mode= BASE_AFF;
            break;
        case CURRENT_PROG:
             mode= BASE_AFF;
            break;
        case LUM :
             mode= BASE_AFF;
            break;
        case TEST_COM:
            EUSART1_stringWrite("ok");
            mode =0 ;
        break;
        case DEBUG:
            while (varDebug !=10){
                varDebug=0;
                
                if(flag_receive){
                    EUSART1_stringRead(&DataIn); 
                    varDebug= atoi(&DataIn.data);
                    flag_receive =0;
                    pass=0;
                }
                switch (varDebug) {
                    case 0 :
                        if(!pass){
                        for (i=0 ; i<= 15; i++)
                            PCA9685_SetLedPattern(i,0, tab_Toff[i]);
                        pass=1;
                        }
                        break;
                    case 1: 
                        for (i=0 ; i<= 15; i++)
                            tab_Toff[i] += 100;
                        varDebug =0 ;
                        break;
                    case 2:
                        for (i=0 ; i<= 15; i++)
                            tab_Toff[i] -= 100;
                        varDebug =0 ;
                        break;
                    case 3:
                        Toff_debug = EUSART1_toIntRead(&DataIn);
                        for (i=0 ; i<= 15; i++)
                            tab_Toff[i]= Toff_debug;
                        pass=0;            
                        varDebug =0 ;
                        break;
                    case 4: 
                        freqPCA += 5;
                        PCA9685_SetPrescaler(freqPCA);
                        varDebug =0 ;
                        break;
                    case 5: 
                        freqPCA -= 5;
                        PCA9685_SetPrescaler(freqPCA);
                        varDebug =0 ;
                        break;
                    case 6:
                        for(i=0; i<16; i++)
                            EUSART1_Write( tab_Toff[i] );
                        EUSART1_Write(CHAR_ENDL);
                        varDebug =0 ;
                        break;
                    case 7 : 
                        freqPCA= EUSART1_toIntRead(&DataIn);
                        PCA9685_SetPrescaler( freqPCA);
                        pass= 0;
                        varDebug= 0;      
                        break;
                    case 8:
                        if (!toggleClignot) toggleClignot= 1;
                        else toggleClignot= 0;
                        varDebug= 0; 
                        break;
                    case 10: 
                        break;
                        
                    default: 
                        EUSART1_stringWrite("error");
                        varDebug= 0;
                        break;
                }
            }
        break;
        default:  
            EUSART1_stringWrite("error");
            for (i=0 ; i<= 15; i++)
                tab_Toff[i] =1000;
            mode =0 ;
        break;
        }
    

#endif
    
    // Toutes les 100ms...
    if (Tic100ms)
        {
        //Envoi d'instruction pour les bandeaux de leds.
        //par exemple
        //PCA9685_SetLedPattern(1,2000, ton,toff)
        Tic100ms = 0;
        }
    
    // Une acquisition de luminosit� a �t� r�alis�e
    if (AcqLum)
        {
        CMD_CAPT_LUM = 0;
        Luminosite = ADRESH;
        
        AcqLum = 0;
        }
    }
}

