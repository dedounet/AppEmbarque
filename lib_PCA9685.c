/***************************************************************************
* Nom du prg 	:   librairie de gestion du PCA9685
* Description	:   
*
* Auteur        :   FT
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:   
***************************************************************************/


#include <stdint.h>

#include "lib_PCA9685.h"

/* D�clarations des variables globales 	*/


/* Directives de compilation		*/
					

/*	Impl�mentation du code */

signed char PCA9685_Reset(void){
    signed char Res = I2C_OK;
     I2C_Start();
     Res= I2C_PutByte(0x00); //adresse de reset , noAck
    if (Res != I2C_OK)
    {
        return Res;  // R�ception ACK ?
    }
    
     Res= I2C_PutByte(0x06); //reset cmd
     I2C_Stop();
     return Res;   
}

void    PCA9685_Init(void)
{
    TRIS_OE = 0;        // Ligne Output Enable en sortie
    PCA9685_OutputDisable();

    I2C_Init();
    
    // Initialisation en mode Low Power et AI = 1 (auto increment)
    //PCA9685_SetMode(PCA_LOW_POWER_MODE);
    PCA9685_WriteReg(REG_MODE1,PCA_LOW_POWER_MODE);
    PCA9685_SetPrescaler(PRESCALER_PCA_9685);
    PCA9685_WriteReg(REG_MODE2, 0x04);
    PCA9685_WriteReg(REG_MODE1,PCA_NORMAL_MODE);
    //PCA9685_SetMode(PCA_NORMAL_MODE);
    PCA9685_OutputEnable();
    
}

unsigned char   PCA9685_OutputEnable(void)
{
LAT_OE = 0;
return 0;
}
unsigned char   PCA9685_OutputDisable(void)
{
  LAT_OE = 1;
  return 1;
}

void PCA9685_OutputToggle(void)
{
    if (PORT_OE)
        LAT_OE=0;
    else 
        LAT_OE=1;
}


signed char PCA9685_WriteReg(unsigned char RegAdr,unsigned char Data)
{
    signed char Res;

    I2C_Start();

    Res = I2C_PutByte(PCA_ADDR & 0xFE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    Res = I2C_PutByte(RegAdr);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    Res = I2C_PutByte(Data);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    I2C_Stop();
    return Res;

}
					
signed char PCA9685_ReadReg(unsigned char RegAdr,unsigned char *pData)
{
signed char Res;

    I2C_Start();

    Res = I2C_PutByte(PCA_ADDR & 0xFE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    Res = I2C_PutByte(RegAdr);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    I2C_Restart();

    Res = I2C_PutByte(PCA_ADDR | 0x01);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    Res = I2C_GetByte(pData,NO_ACK);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    I2C_Stop();

    return Res;

    
}
					
signed char PCA9685_SetMode(tModePCA Mode)
{
    unsigned char Mode1;
    signed char Res;

    Res = PCA9685_ReadReg(REG_MODE1 ,&Mode1);
    if (Res != I2C_OK) return Res;

    if (Mode == PCA_NORMAL_MODE)
    {
        Mode1 = Mode1 & PCA_NORMAL_MODE;
    }
    else
    {
        Mode1 = Mode1 |  PCA_LOW_POWER_MODE;
    }

    PCA9685_WriteReg(REG_MODE1,Mode1);
    return Res;

}

/*---Frequence des PWM---*/
signed char PCA9685_SetPrescaler(unsigned char Valeur)
{
    signed char Res;
    unsigned char Byte;

    // Lecture registre mode 1
    Res = PCA9685_ReadReg(REG_MODE1,&Byte);

    if ((Byte & 0x10)==PCA_LOW_POWER_MODE)
    {
        // Mode Sleep ok
        Res = PCA9685_WriteReg(REG_PRESCALE,Valeur);
        
    }
    else
    {
        // Le PCA est actif, le basculer en mode sleep d'abord
        Res = PCA9685_SetMode(PCA_LOW_POWER_MODE);

        // Changment prescaler
        Res = PCA9685_WriteReg(REG_PRESCALE,Valeur);
        
        // Retour en mode normal
        Res = PCA9685_SetMode(PCA_NORMAL_MODE);
        
    }

    __delay_ms(1);
    return Res;
    
}


/*----setled pattern : OK----*/
signed char PCA9685_SetLedPattern(unsigned char IdLed,unsigned int ton,unsigned int toff)
{
    unsigned char LedOnL,LedOnH,LedOffL,LedOffH;
    signed char Res;
    unsigned char AdrReg;

    if ((IdLed > 15) || (ton > 4095) || (toff > 4095)) return -1;

    LedOnL = ton & 0x00FF;
    LedOnH = (ton >> 8) & 0x00FF;

    LedOffL = toff & 0x00FF;
    LedOffH = (toff >> 8) & 0x00FF;

    AdrReg = REG_LED_BASE + (IdLed * 4);

    // Communication I2C
    I2C_Start();

    // Emission Adresse PCA
    Res = I2C_PutByte(PCA_ADDR  & 0xFE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    // Emission de l'adresse du registre cible LedxON_L
    Res = I2C_PutByte(AdrReg);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission ton_L
    Res = I2C_PutByte(LedOnL);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission ton_H
    Res = I2C_PutByte(LedOnH);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission toff_L
    Res = I2C_PutByte(LedOffL);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission toff_H
    Res = I2C_PutByte(LedOffH);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }
    I2C_Stop();
    return Res;
}

/*---Utilisation des registre LEDALL : NOK--*/
char PCA9685_LedAll(unsigned int toff){
    char Res=0;
    unsigned char LedOnH,LedOffL,LedOffH;
    LedOnH= 0x10;
    
    LedOffL = toff & 0x00FF;
    LedOffH = (toff >> 8) & 0x00FF;
    
  // Communication I2C
    I2C_Start();

    // Emission Adresse PCA
    Res = I2C_PutByte(PCA_ADDR_WRITE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    // Emission de l'adresse du registre cible LedxON_L
    Res = I2C_PutByte(REG_ALL_LED_ON_L);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission ton_L
    Res = I2C_PutByte(0x00);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission ton_H
    Res = I2C_PutByte(LedOnH);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission toff_L
    Res = I2C_PutByte(LedOffL);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    // Emission toff_H
    Res = I2C_PutByte(LedOffH);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }
  
    
    I2C_Stop();
    return Res;
       
}

