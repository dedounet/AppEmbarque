/***************************************************************************
* Nom du prg 	:   librairie de gestion de la m�moire EEPROM
* Description	:   
*
* Auteur        :   Alexis Rolland
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:   
***************************************************************************/


#include "lib_24FC1025.h"

/* D�clarations des variables globales 	*/


/* Directives de compilation		*/
					

/*	Impl�mentation du code */
void    Initialiser_24FC1025(void)
{
    I2C_Init();
}

signed char Lire_24FC1025(unsigned int StartAddr,unsigned int NbBytesToRead,tBank Bank, tCustomAddr cAddr,unsigned char *pCible)
{
signed char Res;    
unsigned char ControlByte = 0xA0;
unsigned char AdrHigh,AdrLow;
unsigned int Index;

unsigned long EndAddress = StartAddr + NbBytesToRead;
if (EndAddress > 0xFFFF) return END_ADDR_OUT_OF_RANGE;

ControlByte |= (Bank << 3);
ControlByte |= (cAddr << 1); // Adresse en �criture

AdrHigh = (StartAddr >> 8) & 0x00FF;
AdrLow = (StartAddr & 0x00FF);


I2C_Start();
Res = I2C_PutByte(ControlByte & 0xFE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

Res = I2C_PutByte(AdrHigh);
    if (Res != I2C_OK)
    {
    I2C_Stop();
    return Res;  // R�ception ACK ?
    }

Res = I2C_PutByte(AdrLow);
    if (Res != I2C_OK)
    {
    I2C_Stop();
    return Res;  // R�ception ACK ?
    }

// Passage en mode lecture
ControlByte |=  1; // Adresse en lecture
I2C_Restart();

    Res = I2C_PutByte(ControlByte);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    Index = 0;
    while(Index < (NbBytesToRead-1))
        {
        Res = I2C_GetByte(&pCible[Index],ACK);
        Index++;
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }
        }
    
    Res = I2C_GetByte(&pCible[Index],NO_ACK);    // Dernier octet lu sans ACQ (cf doc))
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    I2C_Stop();

    return Res;
}

signed char EcrireOctet_24FC1025(unsigned int ByteAddr,tBank Bank, tCustomAddr cAddr,unsigned char Byte)
{
signed char Res;    
unsigned char ControlByte = 0xA0;
unsigned char AdrHigh,AdrLow;

ControlByte |= (Bank << 3);
ControlByte |= (cAddr << 1); // Adresse en �criture

AdrHigh = (ByteAddr >> 8) & 0x00FF;
AdrLow = (ByteAddr & 0x00FF);


I2C_Start();

Res = I2C_PutByte(ControlByte & 0xFE);
if (Res != I2C_OK)
{
    I2C_Stop();
    return Res;  // R�ception ACK ?
}

Res = I2C_PutByte(AdrHigh);
    if (Res != I2C_OK)
        {
        I2C_Stop();
        return Res;  // R�ception ACK ?
        }

Res = I2C_PutByte(AdrLow);
    if (Res != I2C_OK)
        {
        I2C_Stop();
        return Res;  // R�ception ACK ?
        }
Res = I2C_PutByte(Byte);
    if (Res != I2C_OK)
        {
        I2C_Stop();
        return Res;  // R�ception ACK ?
        }

I2C_Stop();
return Res;

    
    
}

signed char EcrirePage_24FC1025(unsigned int StartAddr,unsigned char NbBytesToWrite,tBank Bank, tCustomAddr cAddr,unsigned char *pData)
{
    unsigned long AdresseFin;
    unsigned char ControlByte = 0xA0;
    unsigned char AdrHigh,AdrLow;
    signed char Res;
    unsigned char i;
    
    if (NbBytesToWrite > 128) return -1;
    
    AdresseFin = StartAddr + NbBytesToWrite;
    if ((StartAddr < 0xFFFF) && (AdresseFin > 0xFFFF)) return -2;
    if (AdresseFin >= 0x20000 ) return -3;
    
    ControlByte |= (Bank << 3);
    ControlByte |= (cAddr << 1); // Adresse en �criture

    AdrHigh = (StartAddr >> 8) & 0x00FF;
    AdrLow = (StartAddr & 0x00FF);
    
    I2C_Start();

    Res = I2C_PutByte(ControlByte & 0xFE);
    if (Res != I2C_OK)
    {
        I2C_Stop();
        return Res;  // R�ception ACK ?
    }

    Res = I2C_PutByte(AdrHigh);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }

    Res = I2C_PutByte(AdrLow);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            }
    
    for (i=0;i<NbBytesToWrite;i++)
    {
        Res = I2C_PutByte(pData[i]);
        if (Res != I2C_OK)
            {
            I2C_Stop();
            return Res;  // R�ception ACK ?
            } 
    }
    
    I2C_Stop();
    return I2C_OK;
}