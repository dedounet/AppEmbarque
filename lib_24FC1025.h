/***************************************************************************
* Nom du prg 	:   librairie de gestion de la m�moire EEPROM
* Description	:   
*
* Auteur        :   Alexis Rolland
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:   
***************************************************************************/

#ifndef LIB_24FC1025_H
#define	LIB_24FC1025_H

#include "lib_i2c_18F26K22.h"


#define     END_ADDR_OUT_OF_RANGE   -66



typedef enum {BANK0 = 0,BANK1 = 1} tBank;
typedef enum {A00 = 0,A01=1,A10=2,A11=3} tCustomAddr;

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi : inutile si bus i2c initialis� par ailleurs               */
/*--------------------------------------------------------------------------*/
void    Initialiser_24FC1025(void);



/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char Lire_24FC1025(unsigned int StartAddr,unsigned int NbBytesToRead,tBank Bank, tCustomAddr cAddr,unsigned char *pCible);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char EcrireOctet_24FC1025(unsigned int ByteAddr,tBank Bank, tCustomAddr cAddr,unsigned char Byte);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description : Ecrit une page, soit 127 octets au maximum                 */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char EcrirePage_24FC1025(unsigned int StartAddr,unsigned char NbBytesToWrite,tBank Bank, tCustomAddr cAddr,unsigned char *pData);

#endif	/* LIB_24FC1025_H */

