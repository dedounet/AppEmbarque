/***************************************************************************
* Nom d'ent�te 	:  lib_i2c_V2.h
* Description	: 
*
* Auteur	: 		Alexis ROLLAND
* Cr�� le	: 		16/11/2007
* Compilateur	: XC8
***************************************************************************
* Modifi� le  	: 09/2015 - Portage PIC18F26K22 et XC8
* 
***************************************************************************/

#ifndef XC_H
#define XC_H
#include <xc.h>
#endif

/* D�clarations des �quivalences #define 	*/
#define	I2C_OK			0
#define	I2C_NO_ACK		-1
#define	I2C_BAD_ADDRESS	-2

#define	ACK				0
#define	NO_ACK			1



/* D�clarations des macro-fonctions	 	*/
	

/* D�clarations des prototypes de fonctions	*/
/*******************************************************************************
 fonction   	:  	I2C_Init
 description	: 	Initialisation MSSP en ma�tre I2C
 					Cfg de base : 100KHz / Quartz = 8MHz
 PE				: 	
 PS				:
 Mode d'emploi  :
*******************************************************************************/
void I2C_Init(void);

/*******************************************************************************
 fonction   	:  	I2C_Write_Byte
 description	: 	Fonction de haut niveau �mettant un octet � l'adresse
 					I2C (8bits) fournie.
 PE				:	adr : Adresse I2C au format 8bits (7 bits + d�calage 
 						  � gauche)
 					Byte : Octet � �mettre sur le bus 
 PS				:	I2C_NO_ACK : Pas d'acquittement de la part de l'esclave.
 					I2C_OK : Op�ration r�ussie.
 Mode d'emploi  :	L'adrese � fournir est soit l'adresse en �criture, soit 
 					l'adresse en lecture du composant.
 					La fonction effectue un masquage sur le bit R/W
*******************************************************************************/
signed char I2C_Write_Byte(unsigned char Adr,unsigned char Byte);

/*******************************************************************************
 fonction   	:  	I2C_Read_Byte
 description	: 	Fonction de haut niveau recevant un octet depuis 
 					l'adresse I2C (8bits) fournie.
 PE				:	adr :  Adresse I2C au format 8bits (7 bits + d�calage 
 						   � gauche)
 					pByte : Pointeur sur l'octet qui recevra le r�sultat
 							de la lecture.
 PS				:	cf I2C_Write_Byte
 Mode d'emploi  :	L'adrese � fournir est soit l'adresse en �criture, soit 
 					l'adresse en lecture du composant.
 					La fonction effectue un masquage sur le bit R/W
*******************************************************************************/
signed char	I2C_Read_Byte(unsigned char Adr,unsigned char *pByte);

/*******************************************************************************
 fonction   	:  	I2C_PutByte
 description	: 	Emission brute d'un octet sur le bus
 PE				:	Byte : Octet � �mettre 
 PS				:	I2C_No_ACK ou I2C_OK
 Mode d'emploi  :	
*******************************************************************************/
signed char	I2C_PutByte(unsigned char Byte);

/*******************************************************************************
 fonction   	:	I2C_GetByte  
 description	: 	R�ception brute d'un octet depuis le bus
 PE				: 	pByte : pointeur sur l'unsigned char o� sera stock� l'octet
 					re�u.
 					EtatAck : Valeur de l'ACK ('0' ou '1') du bit d'acquittement
 					� positionner par le ma�tre.
 PS				:	cd I2C_PutByte
 Mode d'emploi  :	Dans le cas d'une lecture simple, l'ACK est � '1'
 					Dans le cas d'une lecture de plusieurs octets, le dernier
 					ack vaut '1', les pr�c�dents '0'.
*******************************************************************************/
signed char	I2C_GetByte(unsigned char *pByte,unsigned char EtatACK);

/*******************************************************************************
 fonction   	:  	I2C_Start
 description	: 	G�n�ration d'une condition de Start sur le bus
 PE				: 
 PS				:
 Mode d'emploi  :
*******************************************************************************/
void	I2C_Start(void);

/*******************************************************************************
 fonction   	: 	I2C_Stop 
 description	: 	G�n�ration d'une condition de Stop sur le bus
 PE				: 
 PS				:
 Mode d'emploi  :
*******************************************************************************/
void	I2C_Stop(void);

/*******************************************************************************
 fonction   	:	I2C_Restart  
 description	:	G�n�ration d'une condition de ReStart sur le bus 
 PE				: 
 PS				:
 Mode d'emploi  :
*******************************************************************************/
void	I2C_Restart(void);

