#ifndef _LIB_PCA9685_H_
#define _LIB_PCA9685_H_
/***************************************************************************
* Nom du prg 	:   librairie de gestion du PCA9685
* Description	:   
*
* Auteur        :   FT
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:   
***************************************************************************/

#include <xc.h>
#include "lib_i2c_18F26K22.h"

#define     PCA_ADDR_WRITE    0x80            // Adresse format 8 bits
#define     PCA_ADDR    0x80            // Adresse format 8 bits
#define     PCA_ADDR_READ     0x81


#define     TRIS_OE     TRISCbits.TRISC0
#define     LAT_OE      LATCbits.LATC0
#define     PORT_OE     PORTCbits.RC0

#define     PRESCALER_PCA_9685  30

#define     REG_MODE1       0x00
#define     REG_MODE2       0x01
#define     REG_SUBADR1     0x02
#define     REG_SUBADR2     0x03
#define     REG_SUBADR3     0x04
#define     REG_ALLCALLADR  0x05

#define     REG_PRESCALE    0xFE
#define     REG_TESTMODE    0xFF

#define     REG_LED_BASE    0x06

#define     REG_ALL_LED_ON_L    250
#define     REG_ALL_LED_ON_H    251
#define     REG_ALL_LED_OFF_L   252
#define     REG_ALL_LED_OFF_H   253

#define _XTAL_FREQ 8000000

typedef enum {
    PCA_NORMAL_MODE = 0x21 ,
    PCA_LOW_POWER_MODE = 0x10   /* avec allled : 0xA0*/
} tModePCA;


/*--------------------------------------------------------------------------*/
/* Fonction  :    									*/
/* Description :                                   */
/* PE : Aucune                                                              */
/* ps : Aucune                                                              */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
void  PCA9685_Init(void);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char PCA9685_WriteReg(unsigned char RegAdr,unsigned char Data);


/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char PCA9685_ReadReg(unsigned char RegAdr,unsigned char *pData);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char PCA9685_SetMode(tModePCA Mode);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char PCA9685_SetPrescaler(unsigned char Valeur);

/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :      IdLed entre 0 et 15                                            */
/*              ton et tof max = 4095                                       */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
signed char PCA9685_SetLedPattern(unsigned char IdLed,unsigned int ton,unsigned int toff);




/*--------------------------------------------------------------------------*/
/* Fonction  :                                                              */
/* Description :                                                            */
/* PE :                                                                     */
/* ps :                                                                     */
/* Mode d'emploi :                                                          */
/*--------------------------------------------------------------------------*/
unsigned char   PCA9685_OutputEnable(void);

unsigned char   PCA9685_OutputDisable(void);

void PCA9685_OutputToggle(void);

char PCA9685_LedAll(unsigned int toff);

signed char PCA9685_Reset(void);

#endif
