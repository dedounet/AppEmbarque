/***************************************************************************
* Nom du prg 	:   librairie g�n�rale de gestion du panneau
* Description	:   
*
* Auteur        :   FT
* Cr�� le       :   23/02/2018
* Compilateur   :   XC8
***************************************************************************
* Modifi� le  	:
***************************************************************************/


#include "lib_habillage.h"


/* D�clarations des variables globales 	*/
// Valeurs des toff en fonction des intensit�s
uint16_t  Tab_toff_Intensites[16] = {0,340,680,1020,1360,1700,2040,2380,2720,3060,3400,3740,4080}; // A ajuster
extern char Tic100ms;
char   NbMotifsProgActif = 0;
char   ProgrammeActif[NB_MOTIF_MAX][10];
uint8_t  MotifActif = 0;
extern DataUart;

/* Directives de compilation		*/
		

/*	Impl�mentation du code */
void Initialiser(void)
{
#ifdef OSCILLATEUR_INTERNE
// Oscillateur interne � 8MHz    
OSCCON = 0b01100000;    
#endif
    
//---------------------------------------------------
// Pin manager  
//---------------------------------------------------

/**
    LATx registers
    */   
    LATA = 0x00;    
    LATB = 0x00;    
    LATC = 0x00;    

    /**
    TRISx registers
    */    
    TRISA = 0xFF;
    TRISB = 0xE7;
    TRISC = 0xBE;  //3E

    /**
    ANSELx registers
    */   
    ANSELC = 0x24;
    ANSELB = 0x27;
    ANSELA = 0x2F;

    /**
    WPUx registers
    */ 
    WPUB = 0x00;
    INTCON2bits.nRBPU = 1;

//---------------------------------------------------
//  XBEE    
//---------------------------------------------------
// GPIO RESET XBEE
TRIS_RESET_XBEE = 0;
RESET_XBEE =1; //Xbee start


//---------------------------------------------------
// Capteur de luminosit�
//---------------------------------------------------
// GPIO commande ON/OFF acquisition
TRIS_CMD_CAPT_LUM = 0;
CMD_CAPT_LUM = 0;       

// Config CAN
ADCON0 = 0x01;  // Voie AN0
ADCON1 = 0x00;  // Refs = Alims
ADCON2 = 0b00101101;    // Tad = 2�s (/16), ACQT = 12 Tad

T0CON = 0x07;
WRITETIMER0(TIC_5SEC);

//---------------------------------------------------
// Timer 1 pour 1 IRQ toutes les 100ms
T1CON = 0x30;
WRITETIMER1(TIC_BANDEAUX);


//---------------------------------------------------
//PCA init 
PCA9685_Init();

// UART init
EUSART1_Initialize();

   
// Interruptions
RCONbits.IPEN = 1;
INTCONbits.GIEH = 1;
INTCONbits.GIEL = 1;

// Timer 0
INTCON2bits.TMR0IP = 0; // Basse priorit�
INTCONbits.TMR0IF = 0;  // S�curit�
INTCONbits.TMR0IE = 1;

// Timer 1
IPR1bits.TMR1IP = 1;    // Haute priorit�
PIR1bits.TMR1IF = 0;    // S�curit�
PIE1bits.TMR1IE = 1;

// CAN
IPR1bits.ADIP = 0;      // Basse priorit�
PIR1bits.ADIF = 0;
PIE1bits.ADIE = 1;

// Lancement timers
T0CONbits.TMR0ON = 1;
T1CONbits.TMR1ON = 1;

//TODO: Verif connection Xbee test


}
//------------------------------------------------------------------------------
signed char GetAdrProgLumEEPROM(unsigned char NbProg, unsigned int* pAdr)
{
    signed char Res = 0;
    unsigned int Adr;
    if ((NbProg < 1) || (NbProg > 64)) return -1;
    if (NbProg > 32) Res = 1;  // Le prog est en bank 1
    
    Adr = (NbProg - 1) * 2048;
    *pAdr = Adr;
    
    return Res;
}
//------------------------------------------------------------------------------
signed char     LoadProgLumFromEEPROM(unsigned char NbProg)
{
    tBank ProgBank;
    unsigned int AdresseDebut;
    signed char Res;
    unsigned char Index;
    
    if ((NbProg < 1) || (NbProg > 64)) return NB_PROG_OUT_OF_RANGE;
    
    if (NbProg <= 32) ProgBank = BANK0;
    else ProgBank = BANK1;
    
    AdresseDebut = (NbProg - 1) * 2048;
    
    Res = Lire_24FC1025(AdresseDebut,1,ProgBank, A00,&NbMotifsProgActif);
    if (Res != I2C_OK) return -1;
    
    if (NbMotifsProgActif == 0xFF) return EMPTY_PROG ;
    if ((NbMotifsProgActif == 0) || (NbMotifsProgActif > NB_MOTIF_MAX) ) return PROG_SIZE_OUT_OF_RANGE;
    
    // Les tests sont ok, lire la EEPROM
    AdresseDebut++; // Le programme lumineux commence � @+1
    
    for (Index = 0;Index<NbMotifsProgActif;Index++)
    {
        Res = LoadMotifEEPROM(AdresseDebut,ProgBank,ProgrammeActif[Index]);
        if (Res != I2C_OK) return -1;
    }
    
    
    
    return 0;
}
//------------------------------------------------------------------------------
signed char     SaveProgActiveToEEPROM(unsigned char NbProg)
{
    tBank ProgBank;
    unsigned int AdresseDebut;
    signed char Res;
    unsigned char Index;
    unsigned int BytesLeftToWrite;
    unsigned char* PtrProg;
    
    if ((NbProg < 1) || (NbProg > 64)) return NB_PROG_OUT_OF_RANGE;
    
    if (NbProg <= 32) ProgBank = BANK0;
    else ProgBank = BANK1;
    
    AdresseDebut = (NbProg - 1) * 2048;
    
    // Ecriture Taille programme
    EcrireOctet_24FC1025(AdresseDebut,ProgBank, A00,NbMotifsProgActif);
    
    // Icr�mentation de l'adresse de d�part
    AdresseDebut++;
    
    // Initialisation du nombre de motifs du programme
    BytesLeftToWrite = NbMotifsProgActif;
    
    // Init adresse d�part en RAM
    PtrProg = (unsigned char*)ProgrammeActif;
    
    while(BytesLeftToWrite > 128)
    {
        EcrirePage_24FC1025(AdresseDebut,128,ProgBank, A00,PtrProg);
        BytesLeftToWrite -= 128;
        PtrProg += 128;
        AdresseDebut += 128;
    }
    
    EcrirePage_24FC1025(AdresseDebut,BytesLeftToWrite,ProgBank, A00,PtrProg);
    
    return 0;
}
//------------------------------------------------------------------------------
signed char     LoadMotifEEPROM(unsigned int AdrMotif,tBank Bank,unsigned char* pData)
{
    signed char Res;
    
    Res = Lire_24FC1025(AdrMotif,10,Bank, A00,pData);
    
    return Res;
}
//------------------------------------------------------------------------------
signed char     ConvertirRawToMotif(unsigned char IdMotif,tMotif* pMotif)
{
    unsigned char i,j,Byte;
    
    if (IdMotif >= NbMotifsProgActif) return -1;
    
    for (i=0;i<8;i++)
    {
        j=(2*i);
        Byte = ProgrammeActif[IdMotif][i];
        pMotif->Intensites[j] =  ((Byte >> 4) & 0x0F);
        pMotif->Intensites[j+1] =  (Byte & 0x0F);
    }
    
    pMotif->Duree = ProgrammeActif[IdMotif][8];
    pMotif->Duree <<=8;
    pMotif->Duree +=ProgrammeActif[IdMotif][9];
    
    return 0;
}
//------------------------------------------------------------------------------
signed char     GenererMotif(tMotif* pMotif)
{
    unsigned char i;
    
    for (i=0;i<16;i++)
    {
    PCA9685_SetLedPattern(i,0,Tab_toff_Intensites[pMotif->Intensites[i]]);
    }
    
    return 0;
}
//------------------------------------------------------------------------------
signed char LibererProgrammeEEPROM(unsigned char NbProg)
{
    tBank ProgBank;
    unsigned int AdresseDebut;
    signed char Res;
    unsigned char Index;
    
    if ((NbProg < 1) || (NbProg > 64)) return NB_PROG_OUT_OF_RANGE;
    
    if (NbProg <= 32) ProgBank = BANK0;
    else ProgBank = BANK1;
    
    AdresseDebut = (NbProg - 1) * 2048;
    
    return EcrireOctet_24FC1025(AdresseDebut,ProgBank, A00,0xFF);
    
}
//------------------------------------------------------------------------------
signed char TesterPresenceProgrammeEEPROM(unsigned char NbProg)
{
    tBank ProgBank;
    unsigned int AdresseDebut;
    signed char Res;
    unsigned char Index;
    unsigned char Byte;
    
    if ((NbProg < 1) || (NbProg > 64)) return NB_PROG_OUT_OF_RANGE;
    
    if (NbProg <= 32) ProgBank = BANK0;
    else ProgBank = BANK1;
    
    AdresseDebut = (NbProg - 1) * 2048;
    
    Res = Lire_24FC1025(AdresseDebut,1,ProgBank, A00,&Byte);
    if (Res != I2C_OK) return -1;
    
    if (Byte == 0xFF) return EMPTY_PROG;
    if ((Byte == 0) || (Byte > NB_MOTIF_MAX)) return PROG_SIZE_OUT_OF_RANGE;
    
    return PROG_OK;
}
//------------------------------------------------------------------------------

char TestXbee(void){
    DataUart message= {{"+++\r"},0};
    EUSART1_stringWrite(message.data);
    EUSART1_stringRead(&message);
    
    strcpy(message.data, "ATCN\r");
    EUSART1_stringWrite(message.data);
    if (message.sizeData !=3) return 0;   
    else return 1;
}

void videBuffer(DataUart *pBuff){
    uint8_t i=0;
    while (i < TAILLE_BUFFER_UART){
        pBuff->data[i]= '\0'; 
        i++;
    }
}

int TraitementUart(DataUart* pDataUart, tEtat* mode){
    //atoi()
    //traitement ascii to int
    *mode =((tEtat)pDataUart->data[0] - 48);
    if ( (pDataUart->data[1] != CHAR_ENDL )&&( pDataUart->data[1]!= '\0' ))
            *mode += (((tEtat)pDataUart->data[1] - 48 ==0) ? 9 : (uint16_t)pDataUart->data[1] - 48) ;
    
   // if (val >12) return -1; // la valeur ascii n'est pas bonne 

    videBuffer(pDataUart);
    return 0; 
}

					
					
